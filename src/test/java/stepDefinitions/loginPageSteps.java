package stepDefinitions;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class loginPageSteps {
	WebDriver driver;
	WebDriverWait wait;
	String loginUrl,userName, pass,email="automation" + randomNumber() + "@gmail.com";

	@Given("^I open browser$")
	public void iOpenBrowser() {
		driver = new FirefoxDriver();
		driver.get("http://demo.guru99.com/v4/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		wait= new WebDriverWait(driver, 30);

	}

	@Given("^I get login url$")
	public void iGetLoginUrl() {
		 loginUrl = driver.getCurrentUrl();
	}

	@When("^I click to here link$")
	public void iClickToHereLink() {
		driver.findElement(By.xpath("//a[text()='here']")).click();

	}

	@When("^I input to email textbox$")
	public void iInputToEmailTextbox() {

		WebElement emailTextbox = driver.findElement(By.xpath("//input[@name='emailid']"));
		waitForElementVisible(emailTextbox);
		emailTextbox.sendKeys(email);

	}

	@When("^I click to submit button$")
	public void iClickToSubmitButton() {
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();

	}

	@When("^I get to username information$")
	public void iGetToUsernameInformation() {
		WebElement userNameText = driver.findElement(By.xpath("//td[contains(text(),'User ID :')]/following-sibling::td"));
		waitForElementVisible(userNameText);
		userName=userNameText.getText();

	}

	@When("^I get to passwork information$")
	public void iGetToPassworkInformation() {
		WebElement passText = driver.findElement(By.xpath("//td[contains(text(),'Password :')]/following-sibling::td"));
		waitForElementVisible(passText);
		pass=passText.getText();

	}

	@When("^I open to login page$")
	public void iOpenToLoginPage() {
		driver.get(loginUrl);
	}

	@When("^I input username textbox$")
	public void iInputUsernameTextbox() {
WebElement userNameTextbox = driver.findElement(By.xpath("//input[@name='uid']"));
waitForElementVisible(userNameTextbox);
userNameTextbox.sendKeys(userName);
	}

	@When("^I input pass textbox$")
	public void iInputPassTextbox() {
		WebElement PassTextbox = driver.findElement(By.xpath("//input[@name='password']"));
		PassTextbox.sendKeys(pass);
	}

	@When("^I click to submit button at login page$")
	public void iClickToSubmitButtonAtLoginPage() {
		driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
	}

	@Then("^I verify HomePageLoginPage message$")
	public void iVerifyHomePageLoginPageMessage() {
		WebElement messHomePage= driver.findElement(By.xpath("//marquee[text()=\"Welcome To Manager's Page of Guru99 Bank\"]"));
		Assert.assertTrue(messHomePage.isDisplayed());
	}
	@Then("^I close browser$")
	public void iCloseBrowser()  {
	    driver.quit();
	}

	public void waitForElementVisible(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public int randomNumber() {
		Random random = new Random();
		int number = random.nextInt(10000);
		return number;
	}

}
